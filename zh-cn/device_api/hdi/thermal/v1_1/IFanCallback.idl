/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Thermal
 * @{
 *
 * @brief 提供设备温度管理、控制及订阅接口。
 *
 * 热模块为热服务提供的设备温度管理、控制及订阅接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口管理、控制和订阅设备温度。
 *
 * @since 4.0
 * @version 1.1
 */

/**
 * @file IFanCallback.idl
 *
 * @brief 提供风扇故障检测回调。
 *
 * 为热服务提供回调，以获取设备温度和风扇转速的变化。
 *
 * 模块包路径：ohos.hdi.thermal.v1_1
 *
 * 引用：ohos.hdi.thermal.v1_1.ThermalTypes
 *
 * @since 4.0
 * @version 1.1
 */

package ohos.hdi.thermal.v1_1;

import ohos.hdi.thermal.v1_1.ThermalTypes;

/**
 * @brief 代表风扇状态变化的回调。
 *
 * 创建回调对象后，热服务可调用{@link IThermalInterface}接口注册回调，以便订阅风扇状态变化。
 *
 * @since 4.0
 */
[callback] interface IFanCallback {
    /**
     * @brief 回调风扇状态变化。
     *
     * @param event 输入参数，设备的风扇信息，包括设备温度和风扇速度。
     * @see HdfThermalCallbackInfo
     *
     * @since 4.0
     */
    OnFanDataEvent([in] struct HdfThermalCallbackInfo event);
}
/** @} */
