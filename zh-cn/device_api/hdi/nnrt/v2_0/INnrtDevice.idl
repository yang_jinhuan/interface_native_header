/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup NNRt
 * @{
 *
 * @brief NNRt（Neural Network Runtime, 神经网络运行时）是面向AI领域的跨芯片推理计算运行时，作为中间桥梁连通上层AI推理框架和底层加速芯片，实现AI模型的跨芯片推理计算。提供统一AI芯片驱动接口，实现AI芯片驱动接入OpenHarmony。
 *
 * @since 3.2
 * @version 2.0
 */

/**
 * @file INnrtDevice.idl
 *
 * @brief 该文件定义芯片设备相关的接口。
 *
 * 该文件包含对芯片设备的信息查询、AI模型编译接口。
 *
 * 模块包路径：ohos.hdi.nnrt.v2_0;
 *
 * 引用：
 * - ohos.hdi.nnrt.v2_0.NnrtTypes
 * - ohos.hdi.nnrt.v2_0.ModelTypes
 * - ohos.hdi.nnrt.v2_0.IPreparedModel
 *
 * @since 3.2
 * @version 2.0
 */

package ohos.hdi.nnrt.v2_0;

import ohos.hdi.nnrt.v2_0.NnrtTypes;
import ohos.hdi.nnrt.v2_0.ModelTypes;
import ohos.hdi.nnrt.v2_0.IPreparedModel;

/**
 * @brief 定义了与设备相关的接口，实现设备管理和模型编译等操作。
 *
 * 当有多个设备注册时，需要保证设备名称和设备商名称的组合全局唯一。
 *
 * @since 3.2
 * @version 2.0
 */
interface INnrtDevice {
    /**
     * @brief 获取设备名称
     *
     * @param name 设备名称
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    GetDeviceName([out] String name);

    /**
     * @brief 获取设备商名称
     *
     * @param name 设备商名称
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    GetVendorName([out] String name);

    /**
     * @brief 获取设备类型
     *
     * @param deviceType 设备类型，DeviceType枚举定义了可选的设备类型，详细定义请查看{@link DeviceType}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）
     *
     * @since 3.2
     * @version 2.0
     */
    GetDeviceType([out] enum DeviceType deviceType);

    /**
     * @brief 获取设备当前状态
     *
     * @param status 设备当前状态，DeviceStatus枚举定义了可选的设备状态，详细定义请查看{@link DeviceStatus}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）
     *
     * @since 3.2
     * @version 2.0
     */
    GetDeviceStatus([out] enum DeviceStatus status);

    /**
     * @brief 查询设备对指定模型的算子支持程度
     *
     * @param model AI模型，模型结构由Model定义，详细定义请查看{@link Model}。
     * @param ops 算子是否支持列表，算子支持列表的顺序与在model中的顺序要一致。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    GetSupportedOperation([in] struct Model model, [out] boolean[] ops);

    /**
     * @brief 查询设备是否支持以Float16精度运算Float32的模型。
     *
     * @param isSupported 是否支持Float16精度。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    IsFloat16PrecisionSupported([out] boolean isSupported);

    /**
     * @brief 查询设备是否支持性能偏好设置，性能偏好的定义可以参考{@link PerformanceMode}。
     *
     * @param isSupported 是否支持性能偏好设置。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    IsPerformanceModeSupported([out] boolean isSupported);

    /**
     * @brief 查询设备是否支持任务优先级设置，优先级的定义可以参考{@link Priority}。
     *
     * @param isSupported 是否支持性能偏好设置。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    IsPrioritySupported([out] boolean isSupported);

    /**
     * @brief 查询设备是否支持变尺寸输入，变尺寸输入意味着同一个模型的不同次运算输入的形状可以不一样。
     *
     * 如果支持变尺寸输入，模型输入Tensor的形状上用-1标记该维度是否可变。
     *
     * @param isSupported 是否支持变尺寸输入。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    IsDynamicInputSupported([out] boolean isSupported);

    /**
     * @brief 编译模型
     *
     * 如果是变尺寸输入模型，则模型输入的维度信息中至少有一个是-1。
     *
     * @param model 需要编译的模型，Model定义请查看{@link Model}。
     * @param config 编译模型的配置，ModelConfig定义请查看{@link ModelConfig}。
     * @param preparedModel 编译好的模型对象，用于后续的运算，IPreparedModel定义请查看{@link IPreparedModel}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    PrepareModel([in] struct Model model,
                 [in] struct ModelConfig config,
                 [out] IPreparedModel preparedModel);

    /**
     * @brief 查询是否支持模型缓存功能
     *
     * 若支持，则需要实现PrepareModelFromModelCache和ExportModelCache两个接口。
     *
     * @param isSupported 是否支持模型缓存。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    IsModelCacheSupported([out] boolean isSupported);

    /**
     * @brief 加载模型缓存，该模型缓存是通过ExportModelCache接口导出的。
     *
     * @param modelCache 模型缓存文件的数组，数组顺序与导出时的数组顺序一致，数组元素类型请查看SharedBuffer定义{@link SharedBuffer}。
     * @param config 加载模型缓存的配置，配置参数的详细定义请参考{@link ModelConfig}。
     * @param preparedModel 加载缓存得到的模型对象，用于后续的运算，IPreparedModel定义请查看{@link IPreparedModel}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    PrepareModelFromModelCache([in] struct SharedBuffer[] modelCache,
                               [in] struct ModelConfig config,
                               [out] IPreparedModel preparedModel);

    /**
     * @brief 加载离线模型文件的缓存，该离线模型是由推理框架传入NNRt并由NNRt解析得到的。
     *
     * @param modelCache 离线模型文件缓存的数组，元素顺序与用户传入的离线模型格式有关，元素类型请查看SharedBuffer定义{@link SharedBuffer}。
     * @param config 加载离线模型文件缓存的配置，配置参数的详细定义请参考{@link ModelConfig}。
     * @param preparedModel 加载离线模型文件缓存得到的模型对象，用于后续的运算，IPreparedModel定义请查看{@link IPreparedModel}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    PrepareOfflineModel([in] struct SharedBuffer[] modelCache,
                        [in] struct ModelConfig config,
                        [out] IPreparedModel preparedModel);

    /**
     * @brief 申请设备共享内存，以文件描述符的形式返回，共享内存主要用于推理输入输出数据的快速传递。
     *
     * @param length 申请共享内存的大小，单位是字节。
     * @param buffer 共享内存的信息，包含共享内存的文件描述符和空间大小，SharedBuffer定义请查看{@link SharedBuffer}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    AllocateBuffer([in] unsigned int length, [out] struct SharedBuffer buffer);

    /**
     * @brief 释放共享内存。
     *
     * @param buffer 共享内存的信息，包含共享内存的文件描述符和空间大小，SharedBuffer定义请查看{@link SharedBuffer}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）。
     *
     * @since 3.2
     * @version 2.0
     */
    ReleaseBuffer([in] struct SharedBuffer buffer);
}

/** @} */