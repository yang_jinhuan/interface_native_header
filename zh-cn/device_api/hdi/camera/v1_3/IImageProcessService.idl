/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

  /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 5.0
 * @version 1.3
 */

/**
 * @file IImageProcessService.idl
 *
 * @brief 声明用于图像处理服务的API。
 *
 * 模块包路径：ohos.hdi.camera.v1_3
 *
 * 引用：
 * - ohos.hdi.camera.v1_2.IImageProcessSession
 * - ohos.hdi.camera.v1_3.IImageProcessCallback 
 * - ohos.hdi.camera.v1_2.IImageProcessService
 *
 * @since 5.0
 * @version 1.1
 */
 

package ohos.hdi.camera.v1_3;

import ohos.hdi.camera.v1_2.IImageProcessSession;
import ohos.hdi.camera.v1_3.IImageProcessCallback;
import ohos.hdi.camera.v1_2.IImageProcessService;

/**
 *@brief 声明图像处理进程服务。
 *
 * 创建映像处理会话，注册后台捕获后回调。
 *
 * @since 5.0
 * @version 1.1
 */
interface IImageProcessService extends ohos.hdi.camera.v1_2.IImageProcessService {
    /**
     * @brief 创建映像处理会话。
     *
     * @param userId 用户ID。
     * @param imageProcessCallback 镜像进程回调。有关详细信息，请参阅{@link IImageProcessCallback}。
     * @param imageProcessSession 指示图像处理会话。有关详细信息，请参阅{@link IImageProcessSession}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    CreateImageProcessSessionExt([in] int userId,
        [in] IImageProcessCallback imageProcessCallback,
        [out] IImageProcessSession imageProcessSession);
}
/** @} */