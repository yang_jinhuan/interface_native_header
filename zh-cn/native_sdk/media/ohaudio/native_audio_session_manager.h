/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 12
 * @version 1.0
 */

/**
 * @file native_audio_session_manager.h
 *
 * @brief 声明音频会话管理相关的接口。
 *
 * 包含创建音频会话管理器、激活/停用音频会话、检查音频会话是否已激活，以及监听音频会话停用事件。
 *
 * @library libohaudio.so
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 * @version 1.0
 */

#ifndef NATIVE_AUDIO_SESSION_MANAGER_H
#define NATIVE_AUDIO_SESSION_MANAGER_H

#include "native_audio_common.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 声明音频会话管理器。
 *
 * 用于管理音频会话相关功能。
 *
 * @since 12
 */
typedef struct OH_AudioSessionManager OH_AudioSessionManager;

/**
 * @brief 音频并发模式。
 *
 * @since 12
 */
typedef enum {
    /**
     * @brief 默认使用系统策略。
     */
    CONCURRENCY_DEFAULT = 0,

    /**
     * @brief 和其它正在播放应用进行混音。
     */
    CONCURRENCY_MIX_WITH_OTHERS = 1,

    /**
     * @brief 后来播放应用压低正在播放应用的音量。
     */
    CONCURRENCY_DUCK_OTHERS = 2,

    /**
     * @brief 后来播放应用暂停正在播放应用。
     */
    CONCURRENCY_PAUSE_OTHERS = 3,
} OH_AudioSession_ConcurrencyMode;

/**
 * @brief 音频会话停用原因。
 *
 * @since 12
 */
typedef enum {
    /**
     * @brief 应用焦点被抢占。
     */
    DEACTIVATED_LOWER_PRIORITY = 0,

    /**
     * @brief 应用停流后超时。
     */
    DEACTIVATED_TIMEOUT = 1,
} OH_AudioSession_DeactivatedReason;

/**
 * @brief 音频会话策略。
 *
 * @since 12
 */
typedef struct OH_AudioSession_Strategy {
    /**
     * @brief 音频并发模式。
     */
    OH_AudioSession_ConcurrencyMode concurrencyMode;
} OH_AudioSession_Strategy;

/**
 * @brief 音频会话已停用事件。
 *
 * @since 12
 */
typedef struct OH_AudioSession_DeactivatedEvent {
    /**
     * @brief 音频会话停用原因。
     */
    OH_AudioSession_DeactivatedReason reason;
} OH_AudioSession_DeactivatedEvent;

/**
 * @brief 这个函数指针将指向用于监听音频会话停用事件的回调函数。
 *
 * @param event 指向{@link OH_AudioSession_Deactivated_Event}音频会话已停用事件。
 * @since 12
 */
typedef int32_t (*OH_AudioSession_DeactivatedCallback) (
    OH_AudioSession_DeactivatedEvent event);

/**
 * @brief 获取音频会话管理器。
 *
 * 使用音频会话管理器相关功能，首先需要获取音频会话管理器实例。
 *
 * @param audioSessionManager 指向{@link OH_AudioSessionManager}音频会话管理器实例。
 *
 * @return 函数返回值：
 *         {@link AUDIOCOMMON_RESULT_SUCCESS} 函数执行成功。
 * @since 12
 */
OH_AudioCommon_Result OH_AudioManager_GetAudioSessionManager(
    OH_AudioSessionManager **audioSessionManager);

/**
 * @brief 激活音频会话。
 *
 * @param audioSessionManager 指向{@link OH_AudioManager_GetAudioSessionManager}创建的音频会话管理实例：{@link OH_AudioSessionManager}。
 * @param strategy 指向{@link OH_AudioSession_Strategy}用于设置音频会话策略。
 * @return 函数返回值：
 *         {@link AUDIOCOMMON_RESULT_SUCCESS} 函数执行成功。
 *         {@link AUDIOCOMMON_RESULT_ERROR_INVALID_PARAM}：
 *                                                        1. 参数audioSessionManager为nullptr；
 *                                                        2. 参数strategy无效。
 *         {@link AUDIOCOMMON_RESULT_ERROR_ILLEGAL_STATE} 非法状态。
 * @since 12
 */
OH_AudioCommon_Result OH_AudioSessionManager_ActivateAudioSession(
    OH_AudioSessionManager *audioSessionManager, const OH_AudioSession_Strategy *strategy);

/**
 * @brief 停用音频会话。
 *
 * @param audioSessionManager 指向{@link OH_AudioManager_GetAudioSessionManager}创建的音频会话管理实例：{@link OH_AudioSessionManager}。
 * @return 函数返回值：
 *         {@link AUDIOCOMMON_RESULT_SUCCESS} 函数执行成功。
 *         {@link AUDIOCOMMON_RESULT_ERROR_INVALID_PARAM}：参数audioSessionManager为nullptr。
 *         {@link AUDIOCOMMON_RESULT_ERROR_ILLEGAL_STATE} 非法状态。
 * @since 12
 */
OH_AudioCommon_Result OH_AudioSessionManager_DeactivateAudioSession(
    OH_AudioSessionManager *audioSessionManager);

/**
 * @brief 检查音频会话是否已激活。
 *
 * @param audioSessionManager 指向{@link OH_AudioManager_GetAudioSessionManager}创建的音频会话管理实例：{@link OH_AudioSessionManager}。
 * @return 用于返回当前应用的音频会话是否已激活，true表示已激活，false表示已停用。
 * @since 12
 */
bool OH_AudioSessionManager_IsAudioSessionActivated(
    OH_AudioSessionManager *audioSessionManager);

/**
 * @brief 注册音频会话停用事件回调。
 *
 * @param audioSessionManager 指向{@link OH_AudioManager_GetAudioSessionManager}创建的音频会话管理实例
 * {@link OH_AudioSessionManager}。
 * @param callback 指向{@link OH_AudioSessionDeactivatedCallback} 用于接收音频会话已停用事件。
 * @return 函数返回值：
 *         {@link AUDIOCOMMON_RESULT_SUCCESS} 函数执行成功。
 *         {@link AUDIOCOMMON_RESULT_ERROR_INVALID_PARAM}：
 *                                                        1. 参数audioSessionManager为nullptr；
 *                                                        2. 参数callback为nullptr。
 * @since 12
 */
OH_AudioCommon_Result OH_AudioSessionManager_RegisterSessionDeactivatedCallback(
    OH_AudioSessionManager *audioSessionManager, OH_AudioSession_DeactivatedCallback callback);

/**
 * @brief 取消注册音频会话停用事件回调。
 *
 * @param audioSessionManager 指向{@link OH_AudioManager_GetAudioSessionManager}创建的音频会话管理实例
 * {@link OH_AudioSessionManager}。
 * @param callback 指向{@link OH_AudioSessionDeactivatedCallback} 用于接收音频会话已停用事件。
 * @return 函数返回值：
 *         {@link AUDIOCOMMON_RESULT_SUCCESS} 函数执行成功。
 *         {@link AUDIOCOMMON_RESULT_ERROR_INVALID_PARAM}：
 *                                                        1. 参数audioSessionManager为nullptr；
 *                                                        2. 参数callback为nullptr。
 * @since 12
 */
OH_AudioCommon_Result OH_AudioSessionManager_UnregisterSessionDeactivatedCallback(
    OH_AudioSessionManager *audioSessionManager, OH_AudioSession_DeactivatedCallback callback);
#ifdef __cplusplus
}
#endif
/** @} */
#endif // NATIVE_AUDIO_SESSION_MANAGER_H