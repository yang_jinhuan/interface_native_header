/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_INPUTMETHOD_CONTROLLER_CAPI_H
#define OHOS_INPUTMETHOD_CONTROLLER_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_controller_capi.h
 *
 * @brief 提供绑定、解绑输入法的方法。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */

#include <stdint.h>
#include <stdlib.h>

#include "inputmethod_text_editor_proxy_capi.h"
#include "inputmethod_inputmethod_proxy_capi.h"
#include "inputmethod_attach_options_capi.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/**
 * @brief 将应用绑定到输入法服务。
 *
 * @param textEditorProxy 表示指向{@link InputMethod_TextEditorProxy}实例的指针。
 *     调用者需要自行管理textEditorProxy的生命周期。
 *     并且如果调用成功，调用者在下次发起绑定或解绑之前，不能将textEditorProxy释放。
 * @param options 表示指向{@link InputMethod_AttachOptions}实例的指针，是绑定时的选项。
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。生命周期维持到下一次绑定或解绑的调用。
 * @return 返回一个特定的错误码.
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_PARAMCHECK} - 表示参数错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodController_Attach(InputMethod_TextEditorProxy *textEditorProxy,
    InputMethod_AttachOptions *options, InputMethod_InputMethodProxy **inputMethodProxy);

/**
 * @brief 将应用从输入法服务解绑。
 *
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。
 *     inputMethodProxy由调用{@link OH_InputMethodController_Attach}获取。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 表示输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 表示输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodController_Detach(InputMethod_InputMethodProxy *inputMethodProxy);
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // OHOS_INPUTMETHOD_CONTROLLER_CAPI_H