/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup rawfile
 * @{
 *
 * @brief Provides the function of operating rawfile directories and rawfiles.
 *
 * These functions include traversing, opening, searching, reading, and closing rawfiles.
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file raw_dir.h
 *
 * @brief Provides functions for operating rawfile directories.
 *
 * These functions include traversing and closing rawfile directories.
 *
 * @since 8
 * @version 1.0
 */
#ifndef GLOBAL_RAW_DIR_H
#define GLOBAL_RAW_DIR_H

#ifdef __cplusplus
extern "C" {
#endif

struct RawDir;

/**
 * @brief Provides the function of accessing rawfile directories.
 *
 *
 *
 * @since 8
 * @version 1.0
 */
typedef struct RawDir RawDir;

/**
 * @brief Obtains the rawfile name via an index.
 *
 * You can use this function to traverse a rawfile directory.
 *
 * @param rawDir Indicates the pointer to {@link RawDir}.
 * @param index Indicates the index of the file in {@link RawDir}.
 * @return Returns the rawfile name via an index. The return value can be used as the input parameter of {@link OH_ResourceManager_OpenRawFile}.
 * If no rawfile is found after all rawfiles are traversed, <b>NULL</b> will be returned.
 * @see OH_ResourceManager_OpenRawFile
 * @since 8
 * @version 1.0
 */
const char *OH_ResourceManager_GetRawFileName(RawDir *rawDir, int index);

/**
 * @brief Obtains the number of rawfiles in {@link RawDir}.
 *
 * You can use this function to obtain available indexes in {@link OH_ResourceManager_GetRawFileName}.
 *
 * @param rawDir Indicates the pointer to {@link RawDir}.
 * @see OH_ResourceManager_GetRawFileName
 * @since 8
 * @version 1.0
 */
int OH_ResourceManager_GetRawFileCount(RawDir *rawDir);

/**
 * @brief Closes an opened {@link RawDir} and releases all associated resources.
 *
 *
 *
 * @param rawDir Indicates the pointer to {@link RawDir}.
 * @see OH_ResourceManager_OpenRawDir
 * @since 8
 * @version 1.0
 */
void OH_ResourceManager_CloseRawDir(RawDir *rawDir);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // GLOBAL_RAW_DIR_H
