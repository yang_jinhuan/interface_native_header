/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AbilityRuntime
 * @{
 *
 * @brief Provides the APIs related to the application-level context.
 *
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

/**
 * @file application_context.h
 *
 * @brief Declares the APIs related to the application-level context.
 *
 * @library libability_runtime.so
 * @kit AbilityKit
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

#ifndef ABILITY_RUNTIME_APPLICATION_CONTEXT_H
#define ABILITY_RUNTIME_APPLICATION_CONTEXT_H

#include <stdint.h>
#include <stddef.h>
#include "ability_runtime_common.h"
#include "context_constant.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Obtains the cache directory of the application-level context.
 *
 * @param buffer Pointer to the buffer, which is used to receive the cache directory.
 * @param bufferSize Buffer size, in bytes.
 * @param writeLength Pointer to the length of the string written to the buffer when
 * {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} is returned.
 * @return Returns one of the following error codes:
 *         {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR}: The operation is successful.
 *         {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID}: The passed-in value of <b>buffer</b> or <b>writeLength</b>
 *         is null, or the buffer size is less than the size of the string to be written.
 * @since 13
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_ApplicationContextGetCacheDir(
    char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
 * @brief Obtains the data encryption level of the application-level context.
 *
 * @param areaMode Pointer to the encryption level of the received data.
 * @return Returns one of the following error codes:
 *         {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR}: The operation is successful.
 *         {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID}: The passed-in value of areaMode is null.
 * @since 13
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_ApplicationContextGetAreaMode(AbilityRuntime_AreaMode* areaMode);

/**
 * @brief Obtains the bundle name of the application.
 *
 * @param buffer Pointer to the buffer, which is used to receive the bundle name.
 * @param bufferSize Buffer size, in bytes.
 * @param writeLength Pointer to the length of the string written to the buffer when
 * {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} is returned.
 * @return Returns one of the following error codes:
 *         {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR}: The operation is successful.
 *         {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID}: The passed-in value of <b>buffer</b> or <b>writeLength</b>
 *         is null, or the buffer size is less than the size of the string to be written.
 * @since 13
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_ApplicationContextGetBundleName(
    char* buffer, int32_t bufferSize, int32_t* writeLength);

#ifdef __cplusplus
} // extern "C"
#endif

/** @} */
#endif // ABILITY_RUNTIME_APPLICATION_CONTEXT_H
