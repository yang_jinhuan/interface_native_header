/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DRAWING_TEXT_FONT_DESCRIPTOR_H
#define DRAWING_TEXT_FONT_DESCRIPTOR_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_font_descriptor.h
 *
 * @brief Declares the capabilities of font information, such as obtaining font information and searching for a font.
 *
 * File to include: "native_drawing/drawing_text_font_descriptor.h"
 * @library libnative_drawing.so
 * @since 14
 * @version 1.0
 */

#include "drawing_text_typography.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines an enum for the system font types.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @since 14
 */
typedef enum OH_Drawing_SystemFontType {
    /** All font types. */
    ALL = 1 << 0,
    /** System font type. */
    GENERIC = 1 << 1,
    /** Style font type. */
    STYLISH = 1 << 2,
    /** User-installed font type. */
    INSTALLED = 1 << 3,
} OH_Drawing_SystemFontType;

/**
 * @brief Obtains all system font descriptors that match a font descriptor.
 * In the {@link OH_Drawing_FontDescriptor} struct, the <b>path</b> field is not used for matching,
 * and other fields are valid only when they are not set to their default values.
 * If all fields in {@link OH_Drawing_FontDescriptor} are set to their default values,
 * all system font descriptors are returned. If no matching is found, NULL is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontDescriptor Pointer to {@link OH_Drawing_FontDescriptor}. You are advised to use
 * {@link OH_Drawing_CreateFontDescriptor} to obtain a valid {@link OH_Drawing_FontDescriptor} instance.
 * If you want to create an {@link OH_Drawing_FontDescriptor} instance, maintain the default values
 * for the fields that are not used for matching.
 * @param size_t Pointer to the number of elements in the array.
 * @return Returns an {@link OH_Drawing_FontDescriptor} array, which must be released by calling
 * {@link OH_Drawing_DestroyFontDescriptors}.
 * @since 14
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_MatchFontDescriptors(OH_Drawing_FontDescriptor*, size_t*);

/**
 * @brief Releases an array of {@link OH_Drawing_FontDescriptor} objects.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontDescriptor Pointer to an array of {@link OH_Drawing_FontDescriptor} objects.
 * @param size_t Number of {@link OH_Drawing_FontDescriptor} objects in the array.
 * @since 14
 * @version 1.0
 */
void OH_Drawing_DestroyFontDescriptors(OH_Drawing_FontDescriptor*, size_t);

/**
 * @brief Obtains a font descriptor based on the font name and type.
 * System fonts, style fonts, and user-installed fonts are supported.
 * A font descriptor is a data structure that describes font features.
 * It contains details of the font appearance and properties.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_String Pointer to the font name, which is {@link OH_Drawing_String}.
 * @param OH_Drawing_SystemFontType Font type, which is defined in {@link OH_Drawing_SystemFontType}.
 * @return Returns the pointer to an {@link OH_Drawing_FontDescriptor} object.
 * @since 14
 */
OH_Drawing_FontDescriptor* OH_Drawing_GetFontDescriptorByFullName(const OH_Drawing_String*, OH_Drawing_SystemFontType);

/**
 * @brief Obtains an array of font names by font type.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_SystemFontType Font type, which is defined in {@link OH_Drawing_SystemFontType}.
 * @return Returns the pointer to an {@link OH_Drawing_Array}, which holds the font names.
 * @since 14
 */
OH_Drawing_Array* OH_Drawing_GetSystemFontFullNamesByType(OH_Drawing_SystemFontType);

/**
 * @brief Obtains the font name with the specified index in the font name array.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Array Pointer to an {@link OH_Drawing_Array} that holds the font names.
 * @param size_t Index of the font in the array.
 * @return Returns the pointer to the font name, which is an {@link OH_Drawing_String} object.
 * @since 14
 */
const OH_Drawing_String* OH_Drawing_GetSystemFontFullNameByIndex(OH_Drawing_Array*, size_t);

/**
 * @brief Releases the memory occupied by the font name array obtained by font type.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Array Pointer to an {@link OH_Drawing_Array} that holds the font names.
 * @since 14
 */
void OH_Drawing_DestroySystemFontFullNames(OH_Drawing_Array*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
